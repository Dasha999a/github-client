package com.example.githubclient

import com.example.githubclient.dataClasses.Repository
import com.example.githubclient.dataClasses.RepositoryDetailed
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.Query

interface GithubService {
    @GET("/orgs/{org}/repos")
    fun listRepos(@Header("Authorization") token: String,
                  @Path("org") organization: String,
                  @Query("per_page") perPage: Int = 100,
                  @Query("page") page: Int) : Call<List<Repository>>

    @GET("repos/{owner}/{repo}")
    fun getDetailsOfRepo(@Header("Authorization") token: String,
                         @Path("owner") owner: String,
                         @Path("repo") repoName: String) : Call<RepositoryDetailed>
}