package com.example.githubclient

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.githubclient.dataClasses.Repository
import com.example.githubclient.dataClasses.RepositoryDetailed
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ViewModelMainActivity : ViewModel() {
    private val retrofit : Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    private val service = retrofit.create(GithubService::class.java)

    var repositories : MutableLiveData<List<Repository>> = MutableLiveData()
    var repoDetails : MutableLiveData<RepositoryDetailed> = MutableLiveData()
    var currentPage = 0
    var pages : Int = 0
    var organizationName = ""

    fun getRepositories(organization: String, context: Context, next : Boolean) {
        CoroutineScope(Dispatchers.IO).launch {
            service.listRepos(TOKEN, organization, page = if (next) ++currentPage else --currentPage)
                .enqueue(object: Callback<List<Repository>> {
                override fun onResponse(
                    call: Call<List<Repository>>,
                    response: Response<List<Repository>>
                ) {
                    if (response.isSuccessful) {
                        repositories.postValue(response.body())
                        val linkHeader = response.headers()["Link"]
                        if (pages == 0) {
                            pages = linkHeader?.get(linkHeader.indexOf("last").minus(9)).
                            toString().toInt()
                        }

                    } else {
                        if (response.body() == null) {
                            repositories.postValue(mutableListOf())
                            if (response.code() == 403) {
                                Toast.makeText(context,
                                    "You have exceeded rate limit",
                                    Toast.LENGTH_LONG)
                                    .show()
                            } else {
                                Toast.makeText(context,
                                    "No repos found for organization $organization",
                                    Toast.LENGTH_LONG)
                                    .show()
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<List<Repository>>, t: Throwable) {
                    Toast.makeText(context,
                        t.localizedMessage,
                        Toast.LENGTH_LONG)
                        .show()
                }
            })
        }
    }

    fun getDetailsOfRepo(owner: String, repoName: String, context: Context) {
        CoroutineScope(Dispatchers.IO).launch {
            service.getDetailsOfRepo(TOKEN, owner, repoName).enqueue(
                object: Callback<RepositoryDetailed> {
                override fun onResponse(
                    call: Call<RepositoryDetailed>,
                    response: Response<RepositoryDetailed>
                ) {
                    if (response.isSuccessful) {
                        repoDetails.postValue(response.body()!!)
                    } else {
                        if (response.code() == 403) {
                            Toast.makeText(context,
                                "You have exceeded rate limit",
                                Toast.LENGTH_LONG)
                                .show()
                        }
                    }
                }

                override fun onFailure(call: Call<RepositoryDetailed>, t: Throwable) {
                    Toast.makeText(context,
                        t.message,
                        Toast.LENGTH_LONG)
                        .show()
                }
            })
        }
    }
}