package com.example.githubclient.dataClasses

import com.google.gson.annotations.SerializedName

data class RepositoryDetailed(
    val name: String,
    var description: String,
    val forks: Int,
    val watchers: Int,
    @SerializedName("open_issues") val openIssues: Int,
    var parent: Parent
)