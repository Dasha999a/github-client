package com.example.githubclient.dataClasses

import com.google.gson.annotations.SerializedName

data class Parent(@SerializedName("full_name") var fullName: String)