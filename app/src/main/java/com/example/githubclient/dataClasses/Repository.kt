package com.example.githubclient.dataClasses

data class Repository(val name: String, val description: String)