package com.example.githubclient.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubclient.R
import com.example.githubclient.RepositoriesAdapter
import com.example.githubclient.ViewModelMainActivity
import com.example.githubclient.dataClasses.Repository
import com.example.githubclient.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity(), OnClickListener {
    private lateinit var binding : ActivityMainBinding
    private lateinit var viewModelMainActivity : ViewModelMainActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        viewModelMainActivity = ViewModelProvider(this)[ViewModelMainActivity::class.java]

        val adapter = RepositoriesAdapter(this)
        val recyclerView = binding.foundRepositories
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        viewModelMainActivity.repositories.observe(this){
            adapter.setData(it)
            adapter.notifyDataSetChanged()
            binding.progressBar.visibility = View.GONE
            if (it != null && it != emptyList<Repository>()) {
                binding.nextPage.visibility = View.VISIBLE
                binding.previousPage.visibility = View.VISIBLE

                binding.nextPage.setOnClickListener {
                    if (viewModelMainActivity.currentPage+1 <= viewModelMainActivity.pages) {
                        binding.progressBar.visibility = View.VISIBLE
                        viewModelMainActivity.getRepositories(
                            viewModelMainActivity.organizationName,
                            applicationContext, true
                        )
                    }
                }

                binding.previousPage.setOnClickListener {
                    if (viewModelMainActivity.currentPage-1 >= 1) {
                        binding.progressBar.visibility = View.VISIBLE
                        viewModelMainActivity.getRepositories(
                            viewModelMainActivity.organizationName,
                            applicationContext, false
                        )
                    }
                }
            }
        }

        binding.searchButton.setOnClickListener {
            viewModelMainActivity.organizationName = binding.organization.text.toString()
            binding.progressBar.visibility = View.VISIBLE
            viewModelMainActivity.currentPage = 0
            viewModelMainActivity.getRepositories(
                viewModelMainActivity.organizationName,
                applicationContext, true)
        }
    }

    override fun onClick(v: View?) {
        Intent(this, DetailsActivity::class.java).also {
            val repoName = v?.findViewById<TextView>(R.id.repository_name)?.text.toString()

            it.putExtra(DetailsActivity.OWNER, binding.organization.text.toString())
            it.putExtra(DetailsActivity.NAME, repoName)
            startActivity(it)
        }
    }
}