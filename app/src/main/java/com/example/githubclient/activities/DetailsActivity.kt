package com.example.githubclient.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.githubclient.ViewModelMainActivity
import com.example.githubclient.databinding.ActivityDetailsBinding


class DetailsActivity : AppCompatActivity() {
    companion object {
        const val OWNER = "owner"
        const val NAME = "name"
    }

    private lateinit var binding : ActivityDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.progressBar.visibility = View.VISIBLE

        val owner = intent.getStringExtra(OWNER)
        val name = intent.getStringExtra(NAME)

        val viewModelMainActivity = ViewModelProvider(this)[ViewModelMainActivity::class.java]
        if (owner != null && name != null) {
            viewModelMainActivity.getDetailsOfRepo(owner, name, this)
        }

        viewModelMainActivity.repoDetails.observe(this) { repo ->
            if (repo != null) binding.progressBar.visibility = View.GONE
            if (repo.parent != null) binding.parent.visibility = View.VISIBLE
            if (repo.description == null) repo.description = "No description message"
            binding.repository = repo
        }
    }
}