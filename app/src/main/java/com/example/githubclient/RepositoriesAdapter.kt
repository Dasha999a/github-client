package com.example.githubclient

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.dataClasses.Repository

class RepositoriesAdapter(private val onClickListener : OnClickListener) : RecyclerView.Adapter<RepositoriesAdapter.ViewHolder>() {

    private var repositories: List<Repository> = emptyList()

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        val repositoryName: TextView = view.findViewById(R.id.repository_name)
        val repositoryDescription: TextView = view.findViewById(R.id.repository_description)
        val view = view
    }

    fun setData(list : List<Repository>) {
        repositories = list
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.repository_view_holder, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val description = repositories[position].description
        val name = repositories[position].name
        holder.view.setOnClickListener(onClickListener)

        holder.repositoryName.text = name
        holder.repositoryDescription.text =
            description ?: "No description message"
    }

    override fun getItemCount() = repositories.size
}